// document - it refers to the whole webpage
// querySelector - it is used to select a specific object (HTML elements)from the documents (webpage)

 const txtFirstName = document.querySelector('#txt-first-name')
 const spanFullName = document.querySelector('#span-full-name')
// const txtLastName = document.querySelector()

// document.getElementById
//document.getElementByClass
//document.getElementByTagName


// Whenever a user interacts with a webpage, this action is considered as an event

txtFirstName.addEventListener('keyup', (event)=> {
    spanFullName.innerHTML = txtFirstName.value;
    console.log(event.target);
    console.log(event.target.value);
})